---
title: "Acknowledgments & Credits for materials used"
---

## Image Credits

* "M51" galaxy picture

  <https://www.nasa.gov/mission_pages/chandra/multimedia/photos07-136.html>



## Datasets

* Untroubled Spam Archive, by Bruce Guenter.

  <http://untroubled.org/spam/>

  <https://archive.org/details/untroubled_spam_archive>  (alternative source)

  "This archive is provided for the purposes of researching behavior
  of spammers and development of new spam management techniques.
  Permission is hereby granted to use this archive without restriction."


## Lessons Materials

* Portions of the lessons were taken from Software Carpentry's UNIX shell
  lesson,

  <http://swcarpentry.github.io/shell-novice>

  The general structure and selection of the UNIX commands introduced
  in this lesson also were drawn from that lesson.

