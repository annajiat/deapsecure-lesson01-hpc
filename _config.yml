#------------------------------------------------------------
# Values for this lesson.
#------------------------------------------------------------

# Which carpentry is this ("swc", "dc", "lc", or "cp")?
# swc: Software Carpentry
# dc: Data Carpentry
# lc: Library Carpentry
# cp: Carpentries (to use for instructor traning for instance)
# odu_ds: ODU DeapSECURE (custom)
carpentry: "odu_ds"
## ^^ FIXME!

# Overall title for pages.
title: "DeapSECURE module 1: Introduction to HPC"

# Life cycle stage of the lesson
# possible values: "pre-alpha", "alpha", "beta", "stable"
life_cycle: "alpha"

#------------------------------------------------------------
# Generic settings (should not need to change).
#------------------------------------------------------------

# What kind of thing is this ("workshop" or "lesson")?
kind: "lesson"

# Magic to make URLs resolve both locally and on GitHub.
# See https://help.github.com/articles/repository-metadata-on-github-pages/.
# Please don't change it: <USERNAME>/<PROJECT> is correct.
repository: <USERNAME>/<PROJECT>

# Email address, no mailto:
email: "deapsecure@odu.edu"

# Sites.
amy_site: "https://amy.software-carpentry.org/workshops"
carpentries_github: "https://github.com/carpentries"
carpentries_pages: "https://carpentries.github.io"
carpentries_site: "https://carpentries.org/"
dc_site: "http://datacarpentry.org"
example_repo: "https://github.com/carpentries/lesson-example"
example_site: "https://carpentries.github.io/lesson-example"
lc_site: "https://librarycarpentry.org/"
swc_github: "https://github.com/swcarpentry"
swc_pages: "https://swcarpentry.github.io"
swc_site: "https://software-carpentry.org"
odu_site: "https://www.odu.edu/"
odu_ds_site: "https://www.odu.edu/ccser/"
template_repo: "https://github.com/carpentries/styles"
training_site: "https://carpentries.github.io/instructor-training"
workshop_repo: "https://github.com/carpentries/workshop-template"
workshop_site: "https://carpentries.github.io/workshop-template"
cc_by_human: "https://creativecommons.org/licenses/by/4.0/"

# DeapSECURE-specific sites
# (root and important addresses)
deapsecure_website: "https://deapsecure.gitlab.io"
deapsecure_hpc_lesson: "https://deapsecure.gitlab.io/deapsecure-lesson01-hpc"
deapsecure_bd_lesson: "https://deapsecure.gitlab.io/deapsecure-lesson02-bd"
deapsecure_ml_lesson: "https://deapsecure.gitlab.io/deapsecure-lesson03-ml"
deapsecure_nn_lesson: "https://deapsecure.gitlab.io/deapsecure-lesson04-nn"
deapsecure_crypt_lesson: "https://deapsecure.gitlab.io/deapsecure-lesson05-crypt"
deapsecure_par_lesson: "https://deapsecure.gitlab.io/deapsecure-lesson06-par"

# Surveys.
swc_pre_survey: "https://www.surveymonkey.com/r/swc_pre_workshop_v1?workshop_id="
swc_post_survey: "https://www.surveymonkey.com/r/swc_post_workshop_v1?workshop_id="
training_post_survey: "https://www.surveymonkey.com/r/post-instructor-training"
dc_pre_survey: "https://www.surveymonkey.com/r/dcpreworkshopassessment?workshop_id="
dc_post_survey: "https://www.surveymonkey.com/r/dcpostworkshopassessment?workshop_id="
lc_pre_survey: "https://www.surveymonkey.com/r/lcpreworkshopsurvey?workshop_id="
lc_post_survey: "https://www.surveymonkey.com/r/lcpostworkshopsurvey?workshop_id="
instructor_pre_survey: "https://www.surveymonkey.com/r/instructor_training_pre_survey?workshop_id="
instructor_post_survey: "https://www.surveymonkey.com/r/instructor_training_post_survey?workshop_id="


# Start time in minutes (0 to be clock-independent, 540 to show a start at 09:00 am).
start_time: 0

# Specify that things in the episodes collection should be output.
collections:
  episodes:
    output: true
    permalink: /:path/index.html
  extras:
    output: true
    permalink: /:path/index.html

# Set the default layout for things in the episodes collection.
defaults:
  - values:
      root: .
      layout: page
  - scope:
      path: ""
      type: episodes
    values:
      root: ..
      layout: episode
  - scope:
      path: ""
      type: extras
    values:
      root: ..
      layout: page

# Files and directories that are not to be copied.
exclude:
  - Makefile
  - bin/
  - .Rproj.user/

# Turn on built-in syntax highlighting.
highlighter: rouge

# Additional values (customizable)

DEAPSECURE_ROOT_URL: http://deapsecure.gitlab.io/deapsecure-lesson01-hpc
DEAPSECURE_ROOT_DIR: /scratch/Workshops/DeapSECURE
DEAPSECURE:
  modules:
    hpc: /scratch/Workshops/DeapSECURE/module-hpc
    bd: /scratch/Workshops/DeapSECURE/module-bd
    ml: /scratch/Workshops/DeapSECURE/module-ml
    nn: /scratch/Workshops/DeapSECURE/module-nn
    crypt: /scratch/Workshops/DeapSECURE/module-crypt
    par: /scratch/Workshops/DeapSECURE/module-par
  datasets:
    untroubled: /scratch/Workshops/DeapSECURE/datasets/untroubled-spam


# local and remote computer names
# (borrowed from Summer 2020 edition of hpc-intro lesson
# by HPC Carpentry)

local:
  prompt: "[user@laptop ~]$"

remote:
  name: "Wahab"
  login: "wahab.hpc.odu.edu"
  host: "wahab-01"
  node: "d1-w6420a-001"
  location: "Old Dominion University"
  basehomedir: "/home"
  userhomedir: "/home/MIDAS_ID"
  userid: "MIDAS_ID"
  #prompt: "[MIDAS_ID@wahab-01 ~]$"
  prompt: "wahab-01:~> "
  appdir: "/shared/apps"

